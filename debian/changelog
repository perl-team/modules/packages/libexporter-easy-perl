libexporter-easy-perl (0.18-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libexporter-easy-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 01:06:55 +0100

libexporter-easy-perl (0.18-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Ben Webb from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 21:49:05 +0100

libexporter-easy-perl (0.18-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 13 Jun 2022 17:27:26 +0200

libexporter-easy-perl (0.18-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata.
  * Import upstream version 0.18.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Mon, 30 Nov 2015 21:12:26 +0100

libexporter-easy-perl (0.17-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Correct Vcs-Browser and Vcs-Git URLs.

  [ gregor herrmann ]
  * debian/copyright: update wording of Comment about copyright
    ownership.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  * New upstream release.
  * debian/copyright: update upstream copyright notice, and Upstream-
    Contact.
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Jul 2014 20:29:44 +0200

libexporter-easy-perl (0.16-1) unstable; urgency=low

  * Initial Release. (closes: #636880)

 -- Ben Webb <bjwebb67@googlemail.com>  Mon, 08 Aug 2011 19:49:36 +0000
